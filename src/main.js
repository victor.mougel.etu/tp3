import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/pizzaForm.js';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];
Router.menuElement = document.querySelector('.mainMenu');

document.querySelector('.logo').innerHTML +=
	"<small>les pizzas c'est la vie</small>";

const news = document.querySelector('.newsContainer');
news.style = 'display:visible';
document.querySelector('.closeButton').addEventListener('click', event => {
	news.style = 'display:none';
});

pizzaList.pizzas = data; // appel du setter
Router.navigate(document.location.pathname); // affiche la liste des pizzas

window.onpopstate = function (event) {
	// console.log(document.location);
	Router.navigate(document.location.pathname);
};
