import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		element
			.querySelector('form')
			.querySelector('button')
			.addEventListener('click', event => {
				event.preventDefault();
				this.submit(element);
			});
	}

	submit(event) {
		const name = event.querySelector('input[name=name]').value;
		if (name.length == 0) window.alert('Invalid pizza name');
		else window.alert(`Pizza ${name} added`);
		event.querySelector('input[name=name]').value = '';
	}
}
