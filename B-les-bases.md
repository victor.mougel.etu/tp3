<img src="images/readme/header-small.jpg" >

# B. Les bases de l'API DOM <!-- omit in toc -->

_**Dans cette partie du TP, nous allons prendre en main les méthodes de base de l'API DOM et notamment celles de sélection et de modification d'éléments de l'arbre DOM.**_

## Sommaire <!-- omit in toc -->
- [B.1. Sélectionner des éléments](#b1-sélectionner-des-éléments)
	- [B.1.1. querySelector()](#b11-queryselector)
	- [B.1.2. querySelectorAll()](#b12-queryselectorall)
- [B.2. Modifier des éléments](#b2-modifier-des-éléments)
	- [B.2.1. innerHTML](#b21-innerhtml)
	- [B.2.2. getAttribute/setAttribute](#b22-getattributesetattribute)


## B.1. Sélectionner des éléments

### B.1.1. querySelector()
**Comme vu en cours, la principale méthode pour sélectionner un élément de la page HTML est la méthode [`querySelector()`](https://developer.mozilla.org/fr/docs/Web/API/Document/querySelector).**

`querySelector()` est une méthode de la classe `Element` qui retourne une référence vers le premier élément de la page (_une balise_) qui correspond au sélecteur CSS passé en paramètre. \
Par exemple :
```js
document.querySelector('.pageContainer');
```
retourne (_si elle existe !_) la balise ayant comme classe CSS `'pageContainer'` :
```html
<section class="pageContainer">
```

**Ouvrez les devtools de votre navigateur, et dans la console, tapez le code suivant :**
```js
document.querySelector('.pizzaList')
```
cette instruction vous retourne la balise qui a comme classe `"pizzaList"`

<img src="images/readme/queryselector-pizzaList.png" >

**Sur le même principe, affichez dans la console :**
1. La balise `<img>` qui contient le logo de la page (les 2 parts de pizza)
2. Le lien du menu "Ajouter une pizza"
3. le titre de la première pizza (`Regina`)

<img src="images/readme/queryselector-console.png">

### B.1.2. querySelectorAll()
**La méthode [`querySelectorAll()`](https://developer.mozilla.org/fr/docs/Web/API/Document/querySelectorAll) permet de récupérer non pas un, mais TOUS les éléments qui correspondent au sélecteur CSS passé en paramètre** (_sous la forme d'un tableau_).

Affichez dans la console :
1. **la liste des liens (`<a href>`) du menu de navigation** ("La carte", "À propos" et "Ajouter une pizza") cf. capture ci-dessous
2. **la liste des `<li>` contenant le prix de toutes les pizzas**

<img src="images/readme/queryselectorall-console.png">

## B.2. Modifier des éléments
### B.2.1. innerHTML
**La propriété [`innerHTML`](https://developer.mozilla.org/fr/docs/Web/API/Element/innertHTML) permet à la fois de lire ET de modifier le contenu d'un Element HTML** (_tout ce qui est compris **entre** les balises ouvrantes et fermantes_)

1. Affichez dans la console le **titre de la deuxième pizza** (_la chaîne de caractères_ `"Napolitaine"`)
2. **Dans le fichier `main.js`, ajoutez au logo** (_grâce à la propriété `innerHTML`_) le code HTML suivant :
	```html
	<small>les pizzas c'est la vie</small>
	```
	De manière à obtenir :
	```html
	<a href="#" class="logo">
		<img src="images/logo.svg" />
		<span>Pizza<em>land</em></span>
		<small>les pizzas c'est la vie</small>
	</a>
	```

	<img src="images/readme/pizzaland-innerhtml.png">

	> _**Rappel :** innerHTML est accessible en écriture ET en lecture !_

### B.2.2. getAttribute/setAttribute
**Les méthodes [`getAttribute()`](https://developer.mozilla.org/fr/docs/Web/API/Element/getAttribute) et [`setAttribute()`](https://developer.mozilla.org/fr/docs/Web/API/Element/setAttribute) de la classe `Element` permettent de lire, d'ajouter ou de modifier des attributs HTML.**

> _**Rappel :** Les **attributs** HTML, ce sont les paires `clé="valeur"` que l'on peut trouver dans les balises ouvrantes (comme `src`, `href`, etc.)._

1. Affichez dans la console **l'url du 2e lien contenu dans le footer** (`"https://www.iut-a.univ-lille.fr/"`)
2. Dans le fichier `main.js`, **ajoutez la classe CSS `"active"`** au premier lien du menu ("La carte")

	<img src="images/readme/pizzaland-setattribute.png">

	> _**NB :** pour associer plusieurs classes CSS à une seule balise, il suffit de les séparer par un espace à l'intérieur de l'attribut `class`. Pour cet exercice on souhaite donc obtenir le code HTML suivant :_
	> ```html
	> <a href="/" class="pizzaListLink active">
	> ```
	> _(Notez l'espace entre "pizzaListLink" et "active")_


## Étape suivante <!-- omit in toc -->
Maintenant que l'on est capable de sélectionner et modifier des éléments HTML, nous allons voir dans le prochain exercice comment détecter des événements : [C. Les événements](./C-evenements.md).